# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(email: 'corey@greylandcreative.com', password: 'WhatYouDo')

Tag.create(tag_name: 'Mystery')
Tag.create(tag_name: 'Romance')
Tag.create(tag_name: 'Adventure')
Tag.create(tag_name: 'Sci-fi')
Tag.create(tag_name: 'Drama')
Tag.create(tag_name: 'Poetry')
Tag.create(tag_name: 'Horror')
Tag.create(tag_name: 'Autobiography')
Tag.create(tag_name: 'Biography')
Tag.create(tag_name: 'Play')