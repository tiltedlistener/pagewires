class CreateTagInstances < ActiveRecord::Migration
  def change
    create_table :tag_instances do |t|
      t.integer :tag_id
      t.integer :idea_id

      t.timestamps null: false
    end
  end
end
