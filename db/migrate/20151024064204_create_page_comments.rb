class CreatePageComments < ActiveRecord::Migration
  def change
    create_table :page_comments do |t|
      t.integer :user_id
      t.integer :page_id
      t.text :comment

      t.timestamps null: false
    end
  end
end
