class CreatePageLikes < ActiveRecord::Migration
  def change
    create_table :page_likes do |t|
      t.integer :page_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
