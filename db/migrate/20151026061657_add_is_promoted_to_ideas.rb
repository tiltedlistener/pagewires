class AddIsPromotedToIdeas < ActiveRecord::Migration
  def change
    add_column :ideas, :isPromoted, :boolean, :default => false
  end
end
