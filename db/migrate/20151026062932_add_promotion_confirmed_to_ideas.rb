class AddPromotionConfirmedToIdeas < ActiveRecord::Migration
  def change
    add_column :ideas, :promotionConfirmed, :boolean, :default => false
  end
end
