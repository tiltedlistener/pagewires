class AddIsAuthorToComment < ActiveRecord::Migration
  def change
    add_column :comments, :is_author, :boolean
  end
end
