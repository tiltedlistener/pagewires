class AddIsAuthorToPageComment < ActiveRecord::Migration
  def change
    add_column :page_comments, :is_author, :boolean
  end
end
