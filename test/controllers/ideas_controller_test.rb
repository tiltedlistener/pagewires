require 'test_helper'

class IdeasControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "All ideas load" do

	get :index, { :format => :json }
	assert_response :success  	

	json = JSON.parse(@response.body)
	assert_equal 2, json.count, "Fetched an incorrect number of ideas"

  end

  test "new idea saves" do 

	assert_equal(Idea.count, 2, "Should be the fixture count")

	post :create, { :idea => "this is a new idea" , :format => :json }
	assert_response :success


	assert_equal(Idea.count, 3, "Should have added a new Idea")

  end
  
end
