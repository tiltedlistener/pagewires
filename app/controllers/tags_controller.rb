class TagsController < ApplicationController

	def index
		tags = Tag.all
  		respond_to do |format|
			format.json  { render :json => tags.to_a }
		end		
	end

	def show		
		id = params[:id]
		unless Tag.where(:id => id).empty?
			tagged_items = TagInstance.where(:tag_id => id)

			@ideas = Array.new
			tagged_items.each do |tag|
				@ideas.push(Idea.find(tag.idea_id))
			end
			@tagName = Tag.find(id).tag_name	
		else 
			redirect_to ideas_path
		end
	end	

end
