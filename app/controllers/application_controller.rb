class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  include SessionsHelper
  include IdeasHelper
  include FirstPagesHelper

  before_filter :set_constants, :set_page_title

  def set_constants 
  	  @controller = params[:controller]
  	  @action = params[:action]
  	  @id = params[:id]
  	  @logged_in = check_user_state()

      if @logged_in 
        @user = get_user()
        @confirmation_needed = check_for_new_promotions(@user.id)
        pages = check_if_has_pages(@user.id)
        @has_pages = pages.count > 0

        @tags = Tag.all

        if @has_pages
          @page_list = pages
        end

      end
  end

  def set_page_title
    page_pattern = 'page_titles.' + params[:controller] + '_' + params[:action]
    @pageTitle = I18n.t(page_pattern, :default => '')
  end

end
