class IdeasController < ApplicationController
	include SessionsHelper
	before_filter :check_session_with_redirect, only: [:create, :destroy, :get_promoted_ideas, :confirm_promotion]

	# CRUD
	def index 
		ideas = Idea.where(isPromoted: false).order("created_at DESC").limit(20)

		tagged_ideas = Array.new
		ideas.each do |idea|
			tagged_ideas.push({ 'idea' => idea, 'tags' => idea.get_tags })
		end  

  		respond_to do |format|
			format.json  { render :json => tagged_ideas }
			format.html
		end		
	end

	def paged
		offset = params[:offset] * 5
		ideas = Idea.offset(offset).order("created_at DESC").limit(5)

  		respond_to do |format|
			format.json  { render :json => ideas.to_a }
			format.html
		end	
	end

	def show 
		@idea = Idea.find(params[:id])
	end

	def create 
		idea = params[:idea][:idea]
		tags = params[:idea][:tags]
		user = User.find_by(session_token: session[:session_token])

		new_idea = Idea.new(idea: idea, user_id: user.id)
		if (new_idea.save)

			unless tags.nil?
				save_tags(new_idea, tags)
			end

			tag_names = Array.new
			tagsAR = new_idea.get_tags
			tagsAR.each do |tag|
				tag_names.push(tag.get_tag_name)
			end

			json_data = { :content => { :idea => new_idea, :tags => tag_names } }
		else 
			json_data = NULL 
		end

  		respond_to do |format|
			format.json  { render :json => json_data }
		end
	end

	def destroy
		idea = Idea.find(params[:id])
		user = User.find_by(session_token: session[:session_token])

		unless idea.nil?
			if (idea.user_id == user.id)
				idea.destroy
			end
		end
		redirect_to user_path
	end

	# Promotion Routes
	def get_promoted_ideas
		user = User.find_by(session_token: session[:session_token])		
		promoted_ideas = user.ideas.where(isPromoted: true, promotionConfirmed: false)

		json_data = {}
		idea_array = Array.new

		promoted_ideas.each do |idea|
			data = {
				:id => idea.id,
				:idea => idea.idea,
				:page_id => idea.pages.first.id,
				:user_id => user.id
			}
			idea_array.push(data)
		end

		if (promoted_ideas.count > 0)
			json_data = { :content => idea_array }
		end
  		respond_to do |format|
			format.json  { render :json => json_data }
		end
	end

	def confirm_promotion
		user = User.find_by(session_token: session[:session_token])
		page_id = params[:id]
		page = user.pages.find(page_id)
		idea = Idea.find(page.idea_id)

		unless idea.nil?
			idea.promotionConfirmed = true;
			idea.save
			json_data = {:state => "success"}
		else
			json_data = {:state => "error"}
		end

  		respond_to do |format|
			format.json  { render :json => json_data }
		end
	end

	private 
		def save_tags(idea, tags) 
			tags.each do |tag|
				idea.tag(tag)
			end
		end

end
