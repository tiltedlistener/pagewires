class FirstPagesController < ApplicationController
	include SessionsHelper
	include FirstPagesHelper

	# CRUD
	def index
		pages = Page.order("created_at DESC").limit(20)

		@pages = Array.new
		@tagsForPages = Array.new
		pages.each do |page|
			unless page.body.nil? || page.body.empty? 
				@pages.push(page)
				@tagsForPages.push(page.lookup_tags)
			end
		end
	end

	def show
		prepare_content		
	end

	def edit
		prepare_content
	end

	def update
		id = params[:id]
		confirm_user_may_enter(id)

		page = Page.find(id)
		if page.update(page_params)
			redirect_to first_page_path(page.id)
		else 
			redirect_to edit_first_page_path(page.id)
		end
	end

	def destroy
		page = Page.find(params[:id])
		user = User.find_by(session_token: session[:session_token])

		unless page.nil?
			if (page.user_id == user.id)
				page.destroy
			end
		end
		redirect_to action: "index"
	end

	private 
		def prepare_content
			id = params[:id]
			confirm_user_may_enter(id)

			@page = Page.find(id)
			@idea = Idea.find(@page.idea_id)
			@tagsForPage = @page.lookup_tags
		end

		def page_params 
			params.permit(:title, :updated_at, :body)
		end

end
