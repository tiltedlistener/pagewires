require 'base64'
require 'digest'

class SessionsController < ApplicationController
	include SessionsHelper

	before_filter :redirect_valid_user, only: [:new]

	def new

	end

	def create
		if user = User.find_by(email: params[:email])
			if user = user.authenticate(params[:password])
				session[:user_id] = user.id
				session[:session_token] = Digest::SHA1.hexdigest([Time.now, rand].join)
				user.session_token = session[:session_token]
				user.save
				redirect_to ideas_path			
			end
		end

		if user.nil?
			flash[:alert] = t(:invalid_creds)
			redirect_to login_path
		end

	end

	def destroy
		if user = User.find_by(session_token: session[:session_token])
			user.session_token = nil
			user.save
		end

		reset_session
		redirect_to root_path
	end

end
