class LikesController < ApplicationController
	include SessionsHelper
	before_filter :check_session_with_redirect

	def show

	end

	def create 
		idea_id = params[:idea_id]
		user = get_user

		existing_like = Like.find_by(idea_id: idea_id, user_id: user.id)

		# If one already exists, we'll delete it
		unless existing_like.nil?
			existing_like.delete
			json_data = {:content => false }
		else
			new_like = Like.new(idea_id: idea_id, user_id: user.id)
			if (new_like.save)
				compute_score(idea_id)				
				json_data = {:content => true }
			else
				json_data = {:content => false }
			end
		end
		
  		respond_to do |format|
			format.json  { render :json => json_data }
		end			

	end

	# Given the current list of ideas, this will check if we like those
	# individual ones. Returns an array that states which ones we DO like
	def check_likes
		ids = params[:ids]
		user = get_user

		final_ids = Array.new
		ids.each do |id|
			if Like.find_by(idea_id: id, user_id: user.id)
				final_ids.push(id)
			end
		end

		json_data = {:content => final_ids }
  		respond_to do |format|
			format.json  { render :json => json_data }
		end		

	end


	# Page Methods
	def like_page
		id = params[:page_id]
		user = get_user

		existing_like = PageLike.find_by(page_id: id, user_id: user.id)

		# If one already exists, we'll delete it
		unless existing_like.nil?
			existing_like.delete
			json_data = {:content => false }
		else
			new_like = PageLike.new(page_id: id, user_id: user.id)
			if (new_like.save)
				json_data = {:content => true }
			else
				json_data = {:content => false }
			end
		end
		
  		respond_to do |format|
			format.json  { render :json => json_data }
		end		
	end

	def check_page 
		id = params[:page_id]
		user = get_user
		existing_like = PageLike.find_by(page_id: id, user_id: user.id)

		unless existing_like.nil?
			json_data = {:content => true }
		else
			json_data = {:content => false }
		end
		
  		respond_to do |format|
			format.json  { render :json => json_data }
		end			
	end

end
