class PageCommentsController < ApplicationController
	include SessionsHelper

	before_filter :check_session_with_redirect, only: [:create]

	def show
		id = params[:id]
		comments = PageComment.where(:page_id => id).order("created_at DESC")

  		respond_to do |format|
			format.json  { render :json => comments.to_a }
		end
	end

	def create
		comment = params[:comment]
		page_id = params[:page_id]
		user = User.find_by(session_token: session[:session_token])

		author_test = false
		if (Page.find(page_id).user_id == user.id) 
			author_test = true
		end

		new_comment = PageComment.new(comment: comment, page_id: page_id, user_id: user.id, is_author: author_test)
		if (new_comment.save)
			json_data = { :content => new_comment }
		else 
			json_data = {}
		end

  		respond_to do |format|
			format.json  { render :json => json_data }
		end		
	end

end
