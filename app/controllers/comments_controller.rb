class CommentsController < ApplicationController
	include SessionsHelper
	include IdeasHelper

	before_filter :check_session_with_redirect, only: [:create]

	def show
		id = params[:id]
		comments = Comment.where(:idea_id => id).order("created_at DESC")

  		respond_to do |format|
			format.json  { render :json => comments.to_a }
		end
	end

	def create
		comment = params[:comment]
		idea_id = params[:idea_id]
		user = User.find_by(session_token: session[:session_token])

		author_test = false
		if (Idea.find(idea_id).user_id == user.id) 
			author_test = true
		end

		new_comment = Comment.new(comment: comment, idea_id: idea_id, user_id: user.id, is_author: author_test)
		if (new_comment.save)
			compute_score(idea_id)
			json_data = { :content => new_comment }
		else 
			json_data = {}
		end

  		respond_to do |format|
			format.json  { render :json => json_data }
		end
	end

end
