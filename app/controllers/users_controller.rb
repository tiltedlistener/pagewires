class UsersController < ApplicationController
	include SessionsHelper
	before_filter :check_session_with_redirect

	def show
		@user = get_user()

		# User Content
		@ideas = @user.ideas

		# User Comments
		@comments = @user.comments
		@ideas_from_comments = Array.new
		@comments.each do |comment|
			@ideas_from_comments.push(comment.idea)
		end

		# User Likes
		@likes = @user.likes
		@ideas_from_likes = Array.new
		@likes.each do |like|
			@ideas_from_likes.push(like.idea)
		end
		
	end
	
end
