angular.module('pageWires')

	.filter('preserveBreaks', function($sce) {
		return function(val) {
			// Keep in mind Rails is sanitizing this as well
        	return $sce.trustAsHtml(val);
    	};
	});

;