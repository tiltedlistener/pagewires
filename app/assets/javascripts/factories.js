angular.module('pageWires')

	.factory('ideas', [
		'$http',
		function($http) {

			// List of Ideas and object instantiation 
			var o = {
				ideas: [

				], 
				offset: 0,
			};

			// List Methods
			o.parseIdeas = function(ideas) {
				var formattedIdeas = [];
				for (var i in ideas) {
					var temp = o.parseIdea(ideas[i]);
					formattedIdeas.push(temp);
				}
				return formattedIdeas;
			};

			o.parseIdea = function(idea) {
				var result = {
					'content': idea.idea.idea, 
					'id' : idea.idea.id,
					'isVoted' : false,
					'tags' : idea.tags,
				};
				return result;
			};

			o.getAll = function(offset) {
				o.offset = offset;

			    return $http.get('/ideas.json').success(function(data){
			      angular.copy(o.parseIdeas(data), o.ideas);
			    });		

				/**	For use with the pager when necessary
			    return $http.post('/paged.json', {'offset': o.offset}).success(function(data){
			      angular.copy(o.parseIdeas(data), o.ideas);
			    });		
			    **/	
			}

			// Creation Methods
			o.create = function(idea, tags) {
				return $http.post('/ideas.json', {'idea' : idea, 'tags' : tags}).success(function(data) {
					if (data.content) {
						o.ideas.unshift(o.parseIdea(data.content));
					} 
				});
			};

			return o;
		}
	])

	.factory('comments', [
		'$http',
		'$log',
		function($http, $log) {

			var o = {
				activeComment: null,
				comments: [

				],
			};

			o.setActiveCommentId = function(id) {
				if (id < 1) {
					$log.error('Incorrect ID!');
					return;
				}
				o.activeComment = id;
			};

			o.resetActiveCommentId = function() {
				o.activeComment = null;
			};

			o.getComments = function() {
				return $http.get('/comments/' + o.activeComment + '.json').success(function(data){
					angular.copy(o.parseComments(data), o.comments);
				});
			};

			o.parseComments = function(comments) {
				var formattedComments = [];
				for (var c in comments) {
					var temp = o.formatComment(comments[c]);
					formattedComments.push(temp);
				}
				return formattedComments;				
			};

			o.formatComment = function(comment) {
				return {'content': comment.comment, 'id' : comment.id, 'is_author' :  comment.is_author };
			};		

			o.create = function(comment) {
				var formattedRequest = {
					'comment' : comment,
					'idea_id': o.activeComment,
				};

				return $http.post('/comments.json', formattedRequest).success(function(data) {
					if (data.content) {
						o.comments.unshift(o.formatComment(data.content));
					}
				});
			};			

			return o;
		}
	])

	.factory('likes', [
		'$http',
		function($http) {

			var o = {
				likesOnPage : [],
			};

			o.checkLikes = function(ideaIds) {
				return $http.post('/likes-current.json', {"ids" : ideaIds});
			};

			o.addLike = function(id) {
				return $http.post('/likes.json', {'idea_id': id});
			};

			return o;

		},
	])

	.factory('pageLikes', [
		'$http',
		function($http) {
			var o = {
				isLiked : false,
			};

			o.checkLike = function(id) {
				return $http.post('/first-page/likes/check.json', {'page_id': id}).success(function(data){
					if (data.content) {
						o.isLiked = true;
					} else {
						o.isLiked = false;
					}
				});
			}

			o.addLike = function(id) {
				return $http.post('/first-page/likes.json', {'page_id': id}).success(function(data){
					if (data.content) {
						o.isLiked = true;
					} else {
						o.isLiked = false;
					}
				});
			};

			return o;
		},
	])

	.factory('promoted', [
		'$http',
		function($http) {

			var o = {
				promoted : [],
				promotedGot : false,
			};

			o.getPromoted = function() {
				return $http.get('/promoted.json').success(function(data){
					if (data.content && !o.promotedGot) {
						o.parsePromoted(data.content);
						o.promotedGot = true;
					}
				});
			};			

			o.parsePromoted = function(promotedData) {
				for (var i = 0; i < promotedData.length; i++) {
					o.parseSinglePromotion(promotedData[i]);
				}
			};

			o.parseSinglePromotion = function(promotion) {
				var result = {
					'id' : promotion.id,
					'idea' : promotion.idea,
					'page_id' : promotion.page_id,
					'user_id' : promotion.user_id,
				}
				o.promoted.push(result);
			};

			o.sendConfirmation = function(id) {
				return $http.post('/promoted/confirmed.json', { 'id' : id});
			}

			return o;
		}
	])

	.factory('pageComments', [
		'$http',
		function($http) {
			var o = {
				'comments' : []
			};

			o.getComments = function(id) {
				return $http.get('/first-page/comments/' + id + '.json').success(function(data){
					angular.copy(o.parseComments(data), o.comments);
				});
			};

			o.parseComments = function(comments) {
				var formattedComments = [];
				for (var c in comments) {
					var temp = o.formatComment(comments[c]);
					formattedComments.push(temp);
				}
				return formattedComments;				
			};

			o.formatComment = function(comment) {
				return {'content': comment.comment, 'id' : comment.id, 'is_author' :  comment.is_author };
			};			

			o.create = function(id, comment) {
				return $http.post('/first-page/comments.json', {'page_id' : id, 'comment' : comment}).success(function(data){
					if (data.content) {
						o.comments.unshift(o.formatComment(data.content));
					}					
				});
			}

			return o;
		}
	])

	.factory('tags', [
		'$http',
		function($http) {

			var o = {
				'tags' : [],
			};

			o.getAll = function() {
				return $http.get('/tags.json').success(function(data){
					angular.copy(o.parseTags(data), o.tags);
				});
			};

			o.parseTags = function(tags) {
				var formattedTags = [];
				for (var t in tags) {
					formattedTags.push(o.formatTag(tags[t]));
				}
				return formattedTags;
			};

			o.formatTag = function(tag) {
				return {
					'tag' : tag.tag_name,
					'id' : tag.id
				};
			};	


			return o;
		}
	])

;