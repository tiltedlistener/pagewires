angular.module('pageWires')

	.controller('IdeaCtrl', [
		'$scope',
		'$sanitize',		
		'ideas',
		'$sce',
		'comments',
		'likes',
		'tags',
		function($scope, $sanitize, ideas, $sce, comments, likes, tags) {

			var lookupTag = function(id) {
				for (var i = 0; i < $scope.tagMeta.length; i++) {
					if ($scope.tagMeta[i].id == id) {
						return $scope.tagMeta[i].tag_name;
					}
				}
				return false;
			}			

			// Loads and sets Ideas already created
			$scope.error = '';
			$scope.hasIdeas = false;
			$scope.offset = 0;
			$scope.canPageLeft = false;
			$scope.canPageRight = true;
			$scope.likes = likes;
			$scope.ideas = ideas.ideas;

			tagList = [];

			tags.getAll().then(function(tagData) {
				$scope.tagMeta = tagData.data;

				ideas.getAll($scope.offset).then(function() {
					
					if ($scope.ideas.length > 0) {
						$scope.hasIdeas = true;

						// Format tags
						for (var currentIdea in ideas.ideas) {
							var currentTags = ideas.ideas[currentIdea].tags;
							var holder = [];

							for(var tag in currentTags) {
								var result = lookupTag(currentTags[tag].tag_id);
								holder.push({ 'tag' : result, 'id': currentTags[tag].tag_id});
							}
							
							ideas.ideas[currentIdea].tags = holder
						}

						var idsForLikes = [];
						for (var currentIdea in ideas.ideas) {
							idsForLikes.push(ideas.ideas[currentIdea].id);
						}

						likes.checkLikes(idsForLikes).then(
							function(data){
								var likeList = data.data.content;
								if (likeList.length > 0) {
									for (var idea in $scope.ideas) {
										if (likeList.indexOf($scope.ideas[idea].id) != -1) {
											$scope.ideas[idea].isVoted = true;
										}
									}
								}
							},
							function(data){
								console.log(data);
						});
					}
				});
			});


			// Load new idea modal		
			$('#newIdeaModal').on('shown.bs.modal', function () {
				$('#ideaTextarea').focus()
			})

			$('#newIdeaModal').on('hidden.bs.modal', function() {
				$scope.error = '';
				$scope.$apply();
			});

			// Provides binding for Idea display
			$scope.addIdea = function() {
				if (!$scope.idea || $scope.idea.trim() === '') {	
					$scope.error = 'Your content is empty';
					return;
				}

				ideas.create({
					idea: $sanitize($scope.idea),
					tags: tagList
				}).then(
					function() {
						$scope.idea = '';
						$scope.error = '';
						$scope.hasIdeas = true;
						$('#newIdeaModal').modal('hide');

						// Reset Tags
						tagList = [];
						$('.tag').removeClass('active');
					}, function(data) {
						$scope.error = data.statusText;
					}
				);
			}

			$scope.setCurrentComment = function(id) {
				comments.setActiveCommentId(id);
			};

			$scope.addLike = function(id) {
				likes.addLike(id).then(function(data) {
					$scope.ideas.filter(function(idea) { 
						if (idea.id == id) idea.isVoted = data.data.content;
					});
				});
			};

			// Pagination Controls
			$scope.paginateRight = function() {
				$scope.offset++;
				$scope.canPageLeft = true;
				ideas.getAll($scope.offset).then(function() {
					$scope.ideas = ideas.ideas;
					if ($scope.ideas.length == 0) {
						$scope.hasIdeas = false;
						$scope.canPageRight = false;
					}
				});
			}

			$scope.paginateLeft = function() {
				$scope.offset--;
				$scope.canPageRight = true;
				if ($scope.offset == 0)  {
					$scope.canPageLeft = false;
				}
				ideas.getAll($scope.offset).then(function() {
					$scope.ideas = ideas.ideas;
					if ($scope.ideas.length > 0) {
						$scope.hasIdeas = true;
					}					
				});				
			}

			$scope.addTag = function(id) {
				var index = tagList.indexOf(id);
				if (index == -1) {
					tagList.push(id);
				} else {
					tagList.splice(index, 1);
				}
			};

		}
	])

	.controller('CreateCommentCtrl', [
		'$scope',
		'$sanitize',		
		'$sce',
		'comments',
		function($scope, $sanitize, $sce, comments) {
			$scope.error = '';
			$scope.hasComments = false;

			$('#newCommentModal').on('show.bs.modal', function() {
				comments.getComments().then(
					function() {
						if (comments.comments.length > 0) {
							$scope.hasComments = true;
							$scope.comments = comments.comments;
						} else {
							$scope.hasComments = false;
						}
					},
					function(data) {
						$scope.error = data.statusText;
					}
				);
			});

			// Modal Controls
			$('#newCommentModal').on('shown.bs.modal', function () {
				$('#commentTextarea').focus()
			})

			$('#newCommentModal').on('hidden.bs.modal', function() {
				comments.resetActiveCommentId();
				$scope.error = '';
				$scope.$apply();
			});

			$scope.addComment = function() {
				if (!$scope.comment || $scope.comment.trim() === '') {	
					$scope.error = 'Your comment is empty';
					return;
				}
				comments.create($scope.comment).then(
					function() {
						$scope.comment = '';
						$scope.hasComments = true;
						$scope.comments = comments.comments;
					},
					function(data){
						console.log(data.statusText);
						$scope.error = 'A server error occurred';
					})
			}

		}
	])

	.controller('FirstPagesAlertCtrl', [
		'$scope',
		'$location',
		'promoted',		
		function($scope, $location, promoted) {
			$scope.error = '';
			$scope.hasError = false;

			$('#alertPromotionModal').on('show.bs.modal', function() {
				promoted.getPromoted().then(
					function(data) {
						$scope.promoted = promoted.promoted;
					},
					function(msg) {
						$scope.error = 'A server error occurred.';
						$scope.hasError = true;
					}
				)
			});

			$scope.confirmedPromotion = function(id) {
				promoted.sendConfirmation(id).then(function() {
					// This route matches up with the Rails route in config/routes.rb
					window.location = '/idea/first-page/' + id + '/edit';
				});
			};

		}
	])


	.controller('FirstPageFeedbackCtrl', [
		'$scope',
		'pageLikes',
		'pageComments',
		function($scope, pageLikes, pageComments) {	
			$scope.error = '';
			$scope.hasComments = false;			

			$scope.init = function(pageId) {
				pageLikes.checkLike(pageId).then(function() {
					$scope.isLiked = pageLikes.isLiked;
				});

				pageComments.getComments(pageId).then(function() {
					if (pageComments.comments.length > 0) {
						$scope.hasComments = true;
						$scope.comments = pageComments.comments;
						setTimeout(function() {$('[data-toggle="tooltip"]').tooltip();}, 500);
					} else {
						$scope.hasComments = false;
					}
				});
			}

			// Modal Controls
			$('#newPageCommentModal').on('shown.bs.modal', function () {
				$('#firstpageCommentTextarea').focus()
			})			
			
			$scope.likePage = function(id) {
				pageLikes.addLike(id).then(function() {
					$scope.isLiked = pageLikes.isLiked;
				});
			}

			$scope.addComment = function(id) {
				pageComments.create(id, $scope.comment).then(
					function(data) {
						$scope.comment = '';
						$scope.error = '';
						$scope.hasComments = true;
						$scope.comments = pageComments.comments;
						$('[data-toggle="tooltip"]').tooltip();   
						$('#newPageCommentModal').modal('hide');
					},
					function(data){
						console.log(data.statusText);
						$scope.error = 'A server error occurred';
					}
				);
 			};

		}
	])

;

;