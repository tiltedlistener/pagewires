class TagInstance < ActiveRecord::Base
	belongs_to :idea
	belongs_to :tag

	def get_tag_name
		id = self.tag_id
		return Tag.find(id).tag_name
	end
end
