class Page < ActiveRecord::Base

	belongs_to :user
	belongs_to :idea

	has_many :page_comments
	has_many :page_likes

	before_destroy :reset_idea

	def lookup_tags
		origin = self.idea
		tags = origin.get_tags_by_name
		return tags
	end

	private 
		def reset_idea
			unless self.idea.nil?
				self.idea.isPromoted = false;
				self.idea.promotionConfirmed = false;
				self.idea.save
			end

			self.page_comments.destroy_all
			self.page_likes.destroy_all			
		end

end
