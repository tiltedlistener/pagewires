class Idea < ActiveRecord::Base
	include ActionView::Helpers::SanitizeHelper

	belongs_to :user
	has_many :comments
	has_many :likes
	has_many :pages
	has_many :tag_instances

	before_save :sanitize_idea, :preserve_linebreaks, :trim_size
	before_destroy :delete_comments

	# Promotion is a one-way gate. Once it's promoted - it is, no undoing
	def promote 
		self.isPromoted = true
		self.save
	end

	def confirmPromotion
		self.promotionConfirmed = true;
		self.save
	end

	def needs_confirmation
		!self.promotionConfirmed && self.isPromoted
	end

	def promoted_id
		if self.isPromoted 
			page = Page.find_by(idea_id: self.id)
			return page.id
		end
		return nil
	end

	def tag(id)
		tag = Tag.find(id)
		instance = tag.tag_instances.create(:idea_id => self.id)
		instance.save
	end

	def get_tags
		return self.tag_instances
	end

	def get_tags_by_name
		tags = self.tag_instances
		tag_names = Array.new
		tags.each do |tag|
			tag_names.push(tag.get_tag_name)
		end
		return tag_names
	end

	def untag(id)
		instance = self.tag_instances.where(tag_id: id)
		instance.destroy
	end

	private 
		def sanitize_idea
			self.idea = strip_tags(self.idea)
			if (self.idea.size > 0) 
				return true
			else 
				return false
			end
		end

		def preserve_linebreaks
			self.idea = idea.gsub(/\n/, '<br/>')
		end

		def trim_size 
			self.idea = self.idea[0, 1600]
		end

		def delete_comments 
			self.comments.destroy_all
			self.tag_instances.destroy_all
			self.likes.destroy_all
			self.pages.destroy_all
		end

end
