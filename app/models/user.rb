class User < ActiveRecord::Base
	has_secure_password

	validates :email, presence: true, uniqueness: true
	validates :password, :length => { :within => 6...20},
						 on: :create

	has_many :ideas
	has_many :comments
	has_many :likes
	has_many :pages
	has_many :page_comments
	has_many :page_likes

	def self.validate_session(id, session)
		if (id != nil && session != nil)
			user = find_by_id(id)
			if (session == user.session_token)
				return true
			else
				return false
			end
		else 
			return false
		end
	end

end
