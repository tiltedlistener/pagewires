module SessionsHelper

	def check_session_with_redirect
		unless User.validate_session(session[:user_id], session[:session_token])
			flash[:alert] = "Please login."
			redirect_to login_path
		end
	end	

	def check_user_state
		return User.validate_session(session[:user_id], session[:session_token])
	end

	# Keep the user out of things like the login page
	def redirect_valid_user
		if User.validate_session(session[:user_id], session[:session_token])
			redirect_to ideas_path
		end
	end	

	def get_user
		if check_user_state
			return User.find(session[:user_id])
		end
		return false
	end
	
end
