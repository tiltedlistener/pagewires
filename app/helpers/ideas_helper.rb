module IdeasHelper
	include SessionsHelper

	def compute_score(id)
		score_goal = 10

		idea = Idea.find(id)
		user = get_user

		if idea.user_id != user.id
			comment_count = idea.comments.size
			like_count = idea.likes.size
			total_score = (comment_count / 2) + like_count

			if (total_score >= score_goal)
				idea.promote

				if (user) 
					page = Page.new(user_id: user.id, idea_id: idea.id)
					page.save
				end
			end
		end
	end

	def check_for_new_promotions(id)
		user = User.find(id)
		ideas = user.ideas

		ideas.each do |idea|
			if (idea.needs_confirmation)
				return true
			end
		end
		return false
	end

end
