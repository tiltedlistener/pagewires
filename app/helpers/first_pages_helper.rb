module FirstPagesHelper
	include SessionsHelper

	# Controller Level
	def confirm_user_may_enter(id)
		check_session_with_redirect
		user = get_user

		page = user.pages.where(:id => id)
		if page.empty? 
			redirect_to login_path
		end
	end

	# Application Level
	def check_if_has_pages(id)
		user = get_user
		return user.pages
	end

end
